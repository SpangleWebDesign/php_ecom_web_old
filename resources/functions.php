<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$upload_directory = "uploads";



function set_message($msg){
    if(!empty($msg)){
        $_SESSION['message'] = $msg;
    } else {
        $msg = "";
    }
}


function display_message(){
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
}


function redirect($location){
	header("Location: $location");
}

function query($sql){
	global $connection;
	return mysqli_query($connection, $sql);
}

function confirm($result){

	global $connection;

	if(!$result)
	{
		die("FAILED" . mysqli_query($connection));
	}
}

function escape_string($string){
	global $connection;

	return mysqli_real_escape_string($connection, $string);
}

function fetch_array($result){
	return mysqli_fetch_array($result);
}


/******************************FRONT END FUNCTIONS**************************************/

// get products

function get_products(){

	$query = query("SELECT * FROM products");
	confirm($query);

	while($row = fetch_array($query)){
        $product_image= display_image($row['product_image']);
		$product = <<<DELIMETER

<div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail">
        <a href="item.php?id={$row['product_id']}"><img width ="100" src="../resources/{$product_image}" alt="">
        <div class="caption"></a>
            <h4 class="pull-right">&#36;{$row['product_price']}</h4>
            <h4><a href="item.php?id={$row['product_id']}">{$row['product_title']}</a>
            </h4>
            <p>{$row['product_description']}</p>
            <a class="btn btn-success" target="_blank" href="../public/checkout.php?add={$row['product_id']}">Add to Cart</a>
        </div>
        
    </div>
</div>

DELIMETER;
echo $product;
	}
}

function get_categories(){
	$query = query("SELECT * FROM categories");
	confirm($query);

	while($row = mysqli_fetch_array($query)){
		$category_links = <<<DELIMETER
<a href = 'category.php?id={$row['cat_id']}'class='list-group-item'>{$row['cat_title']}</a>
DELIMETER;

echo $category_links;
	}
}

function get_product_cat_page(){
	$query = query(" SELECT * FROM products WHERE product_category_id = ". escape_string($_GET['id']). " ");
    confirm($query);
    

    while($row = fetch_array($query)){
        $product_image = display_image($row['product_image']);
    	$product = <<<DELIMETER
<div class="col-md-3 col-sm-6 hero-feature">
    <div class="thumbnail">
        <img width ="100" src="../resources/{$product_image}" alt="">
        <div class="caption">
            <h3>{$row['product_title']}</h3>
            <p>{$row['short_description']}</p>
            <p>
                <a class="btn btn-success" target="_blank" href="../public/checkout.php?add={$row['product_id']}">Add to Cart</a> <a href="item.php?id={$row['product_id']}" class="btn btn-default">More Info</a>
            </p>
        </div>
    </div>
</div>

DELIMETER;
echo $product;
    }
}
/**********************Below here****************************/
function get_product_shop_page(){
	$query = query(" SELECT * FROM products");
    confirm($query);

    while($row = fetch_array($query)){
        $product_image = display_image($row['product_image']);
    	$product = <<<DELIMETER
<div class="col-md-3 col-sm-6 hero-feature">
    <div class="thumbnail">
        <img src="../resources/{$product_image}" alt="">
        <div class="caption">
            <h3>{$row['product_title']}</h3>
            <p>{$row['short_description']}</p>
            <p>
                <a class="btn btn-success" target="_blank" href="../public/checkout.php?add={$row['product_id']}">Add to Cart</a> <a href="item.php?id={$row['product_id']}" class="btn btn-default">More Info</a>
            </p>
        </div>
    </div>
</div>

DELIMETER;
echo $product;
    }
}



function login_user(){

    if(isset($_POST['submit'])){
       $username = escape_string($_POST['username']);
       $password =  escape_string($_POST['password']);

       $query = query("SELECT * FROM users WHERE username = '{$username}' AND password = '{$password}' ");
       confirm($query);

       if(mysqli_num_rows($query) == 0){
        set_message("Your Password or Username is not correct");
        redirect("login.php");        
       } else {
        $_SESSION['username'] = $username;
        //set_message("Welcome to admin {$username}");
        redirect("admin");
       }
    }
}


/*************** Admin Products *************/

function display_image($picture){
    global $upload_directory;
    return $upload_directory . DS . $picture;
}

function get_products_in_admin(){
    $query = query("SELECT * FROM products");
    confirm($query);

    while($row = fetch_array($query)){
        $category = show_product_category_title($row['product_category_id']);
        $product_image= display_image($row['product_image']);
        $product = <<<DELIMETER

    <tr>
        <td>{$row['product_id']}</td>
        <td>{$row['product_title']} <br>
          <a href="index.php?edit_product&id={$row['product_id']}"><img width ="100" src="../../resources/{$product_image}" alt=""></a>
        </td>
        <td>{$category}</td>
        <td>{$row['product_price']}</td>
        <td>{$row['product_quantity']}</td>
        <td>                <a class = "btn btn-danger" href="../../resources/templates/back/delete_product.php?id={$row['product_id']}"><span class='glyphicon glyphicon-remove'></span></a></td>
    </tr>

DELIMETER;
echo $product;
    }


}

function show_product_category_title($product_cat_id){
    $category_query = query("SELECT * FROM categories WHERE cat_id = '{$product_cat_id}'");
    confirm($category_query);

    while($category_row = fetch_array($category_query)){
        return $category_row['cat_title'];
    }
}



/******************* Add Products **********/

function add_product(){

    if(isset($_POST['publish'])){
       $product_title         =  escape_string($_POST['product_title']);
       $product_category_id   =  escape_string($_POST['product_category_id']);
       $product_price         =  escape_string($_POST['product_price']);
       $product_description   =  escape_string($_POST['product_description']);
       $short_desc            =  escape_string($_POST['short_desc']);
       $product_quantity      =  escape_string($_POST['product_quantity']);
       $product_image         =  escape_string($_FILES['file']['name']);
       $image_temp_location   =  $_FILES['file']['tmp_name'];

       move_uploaded_file($image_temp_location, UPLOAD_DIRECTORY . DS . $product_image);

$query = query("INSERT INTO products(product_title, product_category_id, product_price , product_description, product_quantity, product_image, short_description) VALUES ('{$product_title}','{$product_category_id}','{$product_price}','{$product_description}','{$product_quantity}','{$product_image}', '{$short_desc}')");
    confirm($query);

    set_message("New Product Was added");
    redirect("index.php?products");
    }

}

function show_categories_add_product_page(){
    $query = query("SELECT * FROM categories");
    confirm($query);

    while($row = mysqli_fetch_array($query)){
        $category_options = <<<DELIMETER
<option value="{$row['cat_id']}">{$row['cat_title']}</option>

DELIMETER;

echo $category_options;
    }
}


function update_product(){
    if(isset($_POST['update'])){
        $product_title         =  escape_string($_POST['product_title']);
        $product_category_id   =  escape_string($_POST['product_category_id']);
        $product_price         =  escape_string($_POST['product_price']);
        $product_description   =  escape_string($_POST['product_description']);
        $short_description     =  escape_string($_POST['short_desc']);
        $product_quantity      =  escape_string($_POST['product_quantity']);
        $product_image         =  escape_string($_FILES['file']['name']);
        $image_temp_location   =  $_FILES['file']['tmp_name'];

        if(empty($product_image)){

            $get_picture = query("SELECT product_image FROM products WHERE product_id= " .escape_string($_GET['id']). " ");
            confirm($get_picture);

            while($pic = fetch_array($get_picture)){
                $product_image = $pic['product_image'];
            }
        }


        move_uploaded_file($image_temp_location, UPLOAD_DIRECTORY . DS . $product_image);

        $query = "UPDATE products SET ";
        $query .= "product_title          =  '{$product_title}'          , ";
        $query .= "product_category_id    =  '{$product_category_id}'    , ";
        $query .= "product_price          =  '{$product_price}'          , ";
        $query .= "product_quantity       =  '{$product_quantity}'       , ";
        $query .= "product_description    =  '{$product_description}'    , ";
        $query .= "product_image          =  '{$product_image}'          , ";
        $query .= "short_description      =  '{$short_description}'        ";
        $query .= "WHERE product_id=" . escape_string($_GET['id']);

    
    $send_update_query = query($query);
    confirm($send_update_query);
    set_message("The product was updated");
    redirect("index.php?products");

    }
}





//require '../vendor/autoload.php';







function send_message()
{

    if(isset($_POST['submit']))
    {

        $name      =   $_POST['name'];
        $email     =   $_POST['email'];
        $subject    =   $_POST['subject'];
        $message   =   $_POST['message'];


        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
        //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com;';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'm.boland1990@gmail.com';                 // SMTP username
            $mail->Password = 'iloveeggs1';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($email, $name);
            $mail->addAddress('m.boland1990@gmail.com', 'Mitch Boland');     // Add a recipient
            //$mail->addAddress('ellen@example.com');               // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = $message;

            $mail->send();
            echo 'Thanks ' .' '. $name . ',your message has been sent.';
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }
}



/******************************BACK END FUNCTIONS**************************************/


?>