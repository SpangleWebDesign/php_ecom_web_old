<?php require_once("../resources/config.php"); ?>

<?php  include(TEMPLATE_FRONT . DS . "header.php"); ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header >
            <h1>Shop</h1>
        </header>

        
        <!-- Page Features -->
        <div class="row text-center">

            <!--Features will go here-->
            <?php get_product_shop_page(); ?>





        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <?php  include(TEMPLATE_FRONT . DS . "footer.php"); ?>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
